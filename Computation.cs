﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ParallelComputation
{
    public class Computation
    {
        private readonly int[] _array;
        private readonly int _arraySize;
        private readonly Random _randem;
        private readonly Stopwatch _timer;
        private readonly int _endIntRange;
        private readonly int _startIntRange;

        public Computation(int arraySize, int startIntRange, int endIntRange)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Инициализация...");

            _arraySize = arraySize;
            _randem = new Random();
            _endIntRange = endIntRange;
            _startIntRange = startIntRange;
            _array = new int[_arraySize];
            _timer = new Stopwatch();

            Console.WriteLine($"Размер массива: {_arraySize}.");
            Console.WriteLine($"Диапазон складываемых значений: {startIntRange} - {endIntRange - 1}.");

            FillArray();

            Console.ResetColor();
        }

        private void FillArray()
        {
            Console.WriteLine($"Заполнение массива случайными цифрами в диапазоне {_startIntRange} - {_endIntRange - 1}...");
            for (var i = 0; i < _arraySize; i++)
            {
                _array[i] = _randem.Next(_startIntRange, _endIntRange);
            }
            Console.WriteLine($"Заполнение массива случайными цифрами завершено.");
            Console.WriteLine(string.Empty);
        }

        public void Do()
        {
            ThreadComputation();
            Console.WriteLine(string.Empty);
            BaseComputationFor();
            Console.WriteLine(string.Empty);
            BaseComputationForEach();
            Console.WriteLine(string.Empty);
            ParallelComputation();
            Console.WriteLine(string.Empty);
            PLinqComputation();
        }

        private void BaseComputationFor()
        {
            StartProc("Начало стандартного вычисления в цикле for...");

            long result = 0;
            for (var i = 0; i < _arraySize; i++)
            {
                result += _array[i];
            }

            _timer.Stop();
            Console.WriteLine($"Базовое вычисление завершено.\nВремя вычислений составило: {_timer.Elapsed}.\nИтоговая сумма: {result}");
        }

        private void StartProc(string message)
        {
            _timer.Reset();
            _timer.Start();
            Console.WriteLine(message);
        }
        private void BaseComputationForEach()
        {
            StartProc("Начало стандартного вычисления в цикле foreach...");

            long result = 0;
            foreach (var item in _array)
            {
                result += item;   
            }

            _timer.Stop();
            Console.WriteLine($"Базовое вычисление завершено.\nВремя вычислений составило: {_timer.Elapsed}.\nИтоговая сумма: {result}");
        }
        private static object _syncObj = new object();
        private void ParallelComputation()
        {
            StartProc("Начало параллельного вычисления Parallel.ForEach...");

            long result = 0;

            static long Body(int item, ParallelLoopState state, long subtotal) => subtotal += Comp(item);

            void LocalFinally(long subtotal)
            {
                lock (_syncObj)
                {
                    result += subtotal;
                }
            }

            Parallel.ForEach(_array, () => 0L, Body, LocalFinally);

            _timer.Stop();
            Console.WriteLine($"Параллельное вычисление завершено.\nВремя вычислений составило: {_timer.Elapsed}.\nИтоговая сумма: {result}");
        }

        /// <summary> Формирование структуры массива данных от количества процессоров и размера коллекции. </summary>
        private int[][] DefineCollection(int collectionCount, int threadsCount, int collectionEndLen)
        {
            return collectionCount < 100
                ? new int[1][] { _array }
                : collectionEndLen == 0
                    ? new int[threadsCount][]
                    : new int[threadsCount + 1][];
        }

        private void ThreadComputation()
        {
            StartProc("Начало параллельного вычисления Threads без блокировки...");

            var result = 0L;
            var threadsCount = Environment.ProcessorCount;
            var collectionEndLen = _array.Length % threadsCount;
            var curCollection = DefineCollection(_array.Length, threadsCount, collectionEndLen);
            FillDataToCollection(curCollection, threadsCount, collectionEndLen);
            var sumCollection = new int[threadsCount];

            var threads = new Thread[threadsCount];
            for (int i = 0; i < threadsCount; i++)
            {
                threads[i] = new Thread((i) =>
                {
                    var ii = (int)i;
                    for (int j = 0; j < curCollection[ii].Length; j++)
                    {
                        sumCollection[ii] += curCollection[ii][j];
                    }
                });
                threads[i].Start(i);
            }

            for (int i = 0; i < threadsCount; i++)
            {
                threads[i].Join();
            }

            new Thread(() =>
            {
                for (var i = 0; i < sumCollection.Length; i++)
                {
                    result += sumCollection[i];
                }

                _timer.Stop();
                Console.WriteLine($"Параллельное вычисление Threads завершено.\nВремя вычислений составило: {_timer.Elapsed}.\nИтоговая сумма: {result}");
            }).Start();

            Thread.Sleep(100);
        }

        /// <summary> Наполняет данными _curCollection. </summary>
        private void /*int[][]*/ FillDataToCollection(int[][] _curCollection, int threadsCount, int collectionEndLen)
        {
            if (_curCollection.Length == 1)
            {
                _curCollection[0] = _array;
                //return _curCollection;
            }

            var currentSkip = 0;
            var collectionSizeStep = _array.Length / threadsCount;


            for (var j = 0; j < threadsCount; j++)
            {
                _curCollection[j] = _array.Skip(currentSkip).Take(collectionSizeStep).ToArray();
                currentSkip += collectionSizeStep;
            }

            if (collectionEndLen != 0)
            {
                _curCollection[threadsCount] = (int[])_array.Skip(currentSkip).Take(collectionEndLen);
            }

            //return _curCollection;
        }

        private void PLinqComputation()
        {
            StartProc("Начало параллельного вычисления AsParallel...");
            //var result = _array.AsEnumerable<int>().AsParallel().Sum(Comp);
            var result = _array.AsParallel().Sum(Comp);

            _timer.Stop();
            Console.WriteLine($"Параллельное вычисление завершено.\nВремя вычислений составило: {_timer.Elapsed}.\nИтоговая сумма: {result}");
        }
        private static long Comp(int i) => i;
    }
}
