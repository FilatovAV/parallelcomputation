﻿using System;

namespace ParallelComputation
{
    class Program
    {
        static void Main(string[] args)
        {
            var computation1 = new Computation(10_000_000, 1, 1001);
            computation1.Do();
            var computation2 = new Computation(1_000_000, 1, 1001);
            computation2.Do();
            var computation3 = new Computation(100_000, 1, 1001);
            computation3.Do();

            Console.ReadLine();
        }
    }
}
